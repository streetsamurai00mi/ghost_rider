#!/bin/sh

speed=$(sensors | grep cpu_fan | cut -c 14-)

if [ "$speed" != "" ]; then
    speed_round=$(echo "$speed")
    echo "$speed_round"
else
    echo "####"
fi