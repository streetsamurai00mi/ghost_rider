#!/bin/bash

## Created By NgoCongMinh

rofi -modi "Clipboard:greenclip print" -location 5 -xoffset -7 -yoffset -35 -width 35 -line-padding 4 -padding 20 -lines 8 -show Clipboard -run-command '{cmd}'
