#!/bin/sh

monitor_status=$(cat ~/.config/sxhkd/scripts/monitor-status)
externalmonitor=$(xrandr | grep -w 'HDMI1 disconnected')

if [ "$monitor_status" == 'on' ]
then
     xrandr --output eDP1 --off --output HDMI1 --off
     echo "off" > ~/.config/sxhkd/scripts/monitor-status
elif [ -z "$externalmonitor" ]
then
     xrandr --output eDP1 --primary --mode 1920x1080 --rotate normal --output HDMI1 --mode 1366x768 --rotate left --right-of eDP1
     echo "on" > ~/.config/sxhkd/scripts/monitor-status
else
     xrandr --output eDP1 --primary --mode 1920x1080 --rotate normal --output HDMI1 --off
     echo "on" > ~/.config/sxhkd/scripts/monitor-status
fi
