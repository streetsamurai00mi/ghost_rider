#!/bin/bash

function run {
  if ! pgrep $1 ;
  then
    $@&
  fi
}


$HOME/.config/polybar/launch.sh &

##Change X shaped default cursor
xsetroot -cursor_name left_ptr

##Greenclip - Clipboard
systemctl --user start greenclip.service

##hotkey daemon
killall -q sxhkd
while pgrep -u $UID -x sxhkd >/dev/null; do sleep 1; done
sxhkd &

##compton (picom)
killall -q picom
while pgrep -u $UID -x picom >/dev/null; do sleep 1; done
picom --dbus --config $HOME/.config/picom/picom.conf &

##polkit
/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &

##notify daemon
killall -q dunst
while pgrep -u $UID -x dunst >/dev/null; do sleep 1; done
dunst &

##Devilspie
# killall -q devilspie
# while pgrep -u $UID -x devilspie >/dev/null; do sleep 1; done
# devilspie &

##Wallpaper
nitrogen --restore &

##Touchpad gestures
xinput set-prop "ELAN1200:00 04F3:303E Touchpad" "libinput Tapping Enabled" 1 &
libinput-gestures-setup restart &

##Nightmode
killall -q redshift
while pgrep -u $UID -x redshift >/dev/null; do sleep 1; done
redshift -t 6500:2300 -r -l 10.80:106.70 &

##pulseeffects service
pulseeffects --gapplication-service &

##Charge notify
pkill -9 -f battery-notify
sleep 1
./$HOME/battery-notify.sh &

#Keymap
xmodmap .Xmodmap

##Thunderbird tray
killall -q birdtray
while pgrep -u $UID -x birdtray >/dev/null; do sleep 1; done
birdtray &

##Other autostart
xfce4-power-manager &
xfce4-clipman &
telegram-desktop -startintray &
discord --start-minimized &


##Other scripts
###BT
bluetooth_status=$(cat $HOME/.config/polybar/scripts/bluetooth-status)
if [ "$bluetooth_status" == 'on' ]; then
    bluetoothctl power on >> /dev/null
    sleep 1

    devices_paired=$(bluetoothctl paired-devices | grep Device | cut -d ' ' -f 2)
    echo "$devices_paired" | while read -r line; do
        bluetoothctl connect "$line" >> /dev/null
    done
else
    devices_paired=$(bluetoothctl paired-devices | grep Device | cut -d ' ' -f 2)
    echo "$devices_paired" | while read -r line; do
        bluetoothctl disconnect "$line" >> /dev/null
    done

    bluetoothctl power off >> /dev/null
fi

###Touchpad
touchpad_status=$(cat $HOME/.config/sxhkd/scripts/touchpad-status)
if [ "$touchpad_status" == 'off' ]
then
     xinput disable "ELAN1200:00 04F3:303E Touchpad"
else
     xinput enable "ELAN1200:00 04F3:303E Touchpad"
fi
sleep 1

